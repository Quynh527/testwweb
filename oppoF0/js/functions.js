(function($){
	$(document).ready(function (){

		$('.of-iconmntop').click(function(){
			$('.of-menutop').toggleClass('of-mnshow');
		});

		$('.of-search').click(function(){
			$('.of-searchbox').show(200);
		});

		$('.of-closesearch').click(function(){
			$('.of-searchbox').hide(200);
		});
	});
	var swipera = new Swiper('.nextprew1', {
        slidesPerView: 3,
        loop: true,
        effect: 'coverflow',
        loop: true,
        grabCursor: true,
        loop: true,
        loopFillGroupWithBlank: true,
        centeredSlides: true,
        slidesPerView: 3,
        coverflowEffect: {
            rotate: 0,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true
        },
        // init: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.of-slnext',
            prevEl: '.of-slprev',
        },
        breakpoints: {
          1024: {
            slidesPerView: 3,
          },
          768: {
            slidesPerView: 2,
          },
          320: {
            slidesPerView: 1,
          }
        }
	});
	var swipera = new Swiper('.slide2', {
		slidesPerView: 4,
    spaceBetween: 20,
    loop:true,
    autoplay: {
      delay: 500,
      disableOnInteraction: true,
    },
		pagination: {
		 el: '.swiper-pagination',
		  clickable: true,
		},
        breakpoints: {
          768: {
            slidesPerView: 2,
          },
          320: {
            slidesPerView: 1,
          }
        }
	});
	$(window).bind('scroll', function(){
		var navHeight = $(window).height() - 700;
		if($(window).scrollTop() > navHeight){
			$('.menu').addClass('fixed');
		}else{
			$('.menu').removeClass('fixed');
		}
  });
  $(document).on('click', '[toscroll]', function (e) {
    e.preventDefault();
    var link = $(this).attr('toscroll');
    if ($(link).length > 0) {
      var posi = $(link).offset().top - 50;
      $('body,html').animate({ scrollTop: posi }, 1000);
    }
  });
})(window.jQuery);